Date: 8-May-2024

Generics and Collection framework

1:    public static void main(String[] args) {
2:        double a = 75;
3:        double b = 0;
4:        double c = a / b;
5:        System.out.println(c);
6:    }

a)  Compilation error in line 4, requires throws ArithmeticException
b)  Error at runtime at line 4
c)  No error at runtime, what is the output
d)  any other.

jshell

NaN - Not-a-Number

-------------------------------------------------------------
Generics

Type parameters - for reference types only
Data types in Java
primitive       reference

                classes

public class Account<A> {   // generic class, A is a type parameter
    private A accountNumber;
    private String name;
    //
    public A getAccountNumber() {
        return this.accountNumber;
    }
    public void someMethod() {
        this.accountNumber.<any method of Object class>(..);
    }
    public Account(A acno, String name) {
        this.accountNumber = acno;
        this.name = name;
    }
}

Account<Long> ac1;    // accountNumber should be Long
Account<String> ac2;    // accountNumber should be String
ac1 = new Account<>(23544L, "any name");
ac2 = new Account<>("234235", "another name");
String s = ac1.getName();
Long acno1 = ac1.getAccountNumber();
String acno2 = ac2.getAccountNumber();

even interfaces can be generic

public class Pair<K,V> {
    private K key;
    private V value;
    
}

Pair<Integer,String> p1 =


List
    public void add(Object o)
    public Object get(int index)

List<E>
    public void add(E o)
    public E get(int index)


List<String> names = .....

classes and interfaces can be generic
even methods can be generic
public class SomeClass {
    public static <T> void someMethod(T[] array, int index) {
        System.out.println(array[index]);
    }
}
SomeClass.someMethod({"Hello","world"}, 1);

public class GenericClass<T,U> {
    // we cannot use new T(), to invoke a constructor
    // we cannot use new T[5], no array creation
    // cannot be used in a static context
//    public static final T CONSTANT = null;
    public static void main(String[] args) {
//        T and U cannot be used
    }
}

public class Account<A,B> {
    private A accountNumber;
    
    private B balance;
    
}

bounded type parameter
B should be a bounded type parameter

        we also have an interface called Comparable, with a method
        called public int compareTo(

public interface Comparable<T> {
    public int compareTo(T t)
}

Number
    Byte
    Short
    Integer
    Long
    Float
    Double

    BigInteger
    BigDecimal
    
    AtomicLong      // does not implement Comparable

Account<String, Long> ac3

public class Account<B extends Number & Comparable> {  // B is a bounded type parameter
    private long accountNumber;
    
    private B balance;

    public void someMethod() {
        this.balance.<methods of Number and Comparable>
    }
    
}
Account<Double> ac1;

-- sealed types (class and interfaces) -----

permits, sealed and non-sealed are contextual keywords


public sealed interface MyNumberTypes permits MyDouble, MyLong {

}

public final class MyDouble implements MyNumberTypes {

}

public final class MyLong implements MyNumberTypes {

}
sub-types of a sealed types can be sealed, final or non-sealed only

what is Comparable and Comparator
both are generic interfaces

public interface Comparable<T> {
    public int compareTo(T t);

}
This interface is to be implemented type classes, whose elements have
a natural ordering.
The compareTo method defines the natural ordering.

"Hundred" "zero"

class Color

Color.RED   Color.GREEN



a.compareTo(b)  returns < 0 implies a < b, returns > 0 implies a > b,
                returns == 0 implies a is equivalent to b.


comparing two objects using logic other than the natural ordering
then define the other comparision logic in a Comparator interface.

Comparator interface is in java.util package

public interface Comparator<T> {
    public int compare(T t1, T t2);
}

define some class which implements Comparator<T> and create object of
that class.

    c.compare(a, b) // returns < 0 implies a < b according to Comp. c

natural ordering for objects of Account class may be based on 
accountNumber, but we may have Comparator, which compare by name, or by
balance, etc.

about Comparable interface.
all wrapper classes implement the Comparable interface
String implements Comparable interface

public class Employee implements Comparable<Employee> {
    private int empNo;
    public int compareTo(Employee e) {
        return ((Integer)(this.empNo)).compareTo(e.empNo);
    }
}

public class NameComparator implements Comparator<Employee> {
    public int compare(Employee e1, Employee e2) {
        return e1.getName().compareTo(e2.getName());
    }
}

Employee[] employees = .....

Arrays class has static methods called sort.

public static void sort(Object[] array)

public static <T> void sort(T[] array, Comparator<? super T> comp)
you may initially think
public static <T> void sort(T[] array, Comparator<T> comp)

Arrays.sort(employees, new Comparator() {
        public int compare(Employee e1, Employee e2) {
            return e1.getName().compareTo(e2.getName());
        }
    }
);

Arrays.sort(employees, Comparator.comparing(Employee::getName));
Arrays.sort(employees, Comparator.comparingLong(Employee::getSalary));

class Account<A extends Comparable<A>> implements Comparable<Account<A>{
    private A accountNumber;
    public int compareTo(Account<A> ac) {    
        return this.accountNumber.compareTo(ac.accountNumber);
    }
}

Account<Long> ac1;
Account<String> ac2;
ac1.compareTo(ac2) is not valid.

----------------- collection framework -----------------

Collection<E>  extends Iterable<E>
    public boolean add(E e)
    public boolean remove(Object o) // uses equals method
    public boolean contains(Object o) // uses equals method
    public int size()
    public void clear()
    public boolean isEmpty()
    
    public Object[] toArray()
    public <T> T[] toArray(T[] array)

    public Iterator<E> iterator()

    public boolean addAll(Collection<? extends E> c)
    public boolean removeAll(Collection<?> c)   // Collection of any type
    public boolean retainAll(Collection<?> c)
    public boolean containsAll(Collection<?> c)

    a.addAll(b) is like a = a union b
    a.removeAll(b) is like a = a - b
    a.retainAll(b) is like a = a intersection b
    a.containsAll(b) is checking if b is a subset of a



Collection<Account> ca = ...
Collection<SavingsAccount> csa = ....

ca.addAll(csa)

using toArray method.
Collection<String> allNames = ....  // has 15 elements
String[] myStrings = ....;  // suppose it has 10 elements
String[] nameArray = allNames.toArray(myStrings);
    nameArray will be of size 15 with all elements from allNames


using toArray method.
Collection<String> allNames = ....  // has 10 elements
String[] myStrings = ....;  // suppose it has 15 elements
String[] nameArray = allNames.toArray(myStrings);
    nameArray will be of size 15, it is the same array object as
    myStrings. The initial 10 elements will be replaced with the 
    elements from the collection and 11th element will be a null

Collection<String> allNames = ....  // has 10 elements
String[] nameArray = allNames.toArray(new String[allNames.size()]);

some common interfaces from the Collection framework


Iterable<E>
    Collection<E>
        Set<E>
        List<E>
        Queue<E>
        










Iterator<E>
    public boolean hasNext()
    public E next()
    public void remove()

Collection<Account> accountCollection = .....

Iterator<Account> accountIterator = accountCollection.iterator();
while (accountIterator.hasNext()) {
    Account ac = accountIterator.next();
    if (ac.getBalance() < 0) {
//        accountCollection.remove(ac);   
        accountIterator.remove();
    }
    //  process ac
    
}

int[] values = .....

for (int value : values) {
    
}

for (E element : iterable) {
    
}

for (Account ac : accountCollection) {
    //  process ac
    
}



------------------

class GenericClass<T> {
    // T is not available in static class or method
    static class TopLevelNested<U> {
        
    }
    class InnerClass<V> {
        // T is available here
    }
}

class GenericClass<P1,P2> {

}

public static <T> void someGenericMethod(..) {
    GenericClass<String,T> gc1;
    List<Account> la = ...
    List<CurrentAcccount> lca = ....
    List<T> list = ...
    List<?> unbounded = ....    // List of anything
    List<? extends Number> boundedWildcardWithUpperBound = ....
    List<? super CurrentAccount> boundedWildcardWithLowerBound = ...
    List raw = ....

}


-----------------------------------------------------------
Date: 9-May-2024

List<Integer> list = new ArrayList<>();
list.add(45);
//list.remove(45);    // error at runtime
list.remove((Integer)45);

from Java 9, static methods were added to create unmodifiable List easily

List<String> names = List.of("Tom", "Dick", "Harry");

Map is an unordered set of key-value pairs.
a key-value pair is represented by an interface called Map.Entry<K,V>

{1, 2, 3}
{1:"one", 2:"two", 3:"three"}

creating an unmodifiable map easily
Map<Integer,String> digitToWords = Map.ofEntries(
                                Map.entry(1,"one"),
                                Map.entry(2,"two"),
                                Map.entry(3,"three"));






Functional interfaces

interface with single abstract method
ignoring the public methods of the Object class.

public interface Comparable<T> {
    public int compareTo(T t);
    public int hashCode();
}

public non-final methods of the Object class
    public String toString()
    public boolean equals(Object o)
    public int hashCode()

Lambda expression:
implementation of the abstract method of a functional interface
It is an object of the type of a Functional interface

syntax of lambda expression:
(<param-list>) -> { <statements> }

public interface IntBinaryOperator {
    public int applyAsInt(int a, int b);
    
}

IntBinaryOperator f1 = (int a, int b) -> { return a + b; };
IntBinaryOperator f1 = (a, b) -> { return a + b; };
IntBinaryOperator f1 = (a, b) -> a + b;

Object is the super-type for all reference types in Java

Object obj = (IntBinaryOperator)(int a, int b) -> { return a + b; };

IntUnaryOperator f2 = (int a) -> { return a + 5; };
IntUnaryOperator f2 = (a) -> { return a + 5; };
IntUnaryOperator f2 = a -> a + 5;

Functional interface and lambda expression

some of the lambda expression can be represented using a method reference
These lambda expressions involve only single method invocation.

case - 1
(Type1 a, Type2 b, Type3 c) -> a.someMethod(b, c)
the above lambda expression can be written as a method reference:
Type1::someMethod

(Type1 a, Type2 b) -> a.someOtherMethod(b)
Type1:someOtherMethod
method invocation on the first input paratmeter passing rest of the
parameters in the same order.
Account::display
ac -> ac.display()

case-2
static method invocation
(a, b, c) -> Type1.staticMethodOfType1(a, b, c)
the method reference of the above lambda expression will be
Type1::staticMethodOfType1


case-3
method invoked on some object and uses all the paramters in the
same order

(a, b, c) -> someObj.method(a, b, c)
the method referece:
someObj::method
System.out::println
ac -> System.out.println(ac)

constructor reference

(a, b, c) -> new SomeClass(a, b, c)
SomeClass::new
() -> new ArrayList<>()
ArrayList::new

(int size) -> new SomeType[size];
SomeType[]::new

(int size) -> new Account[size];
Account[]::new


IntBinaryOperator f1 = (a, b) -> {
        System.out.println("received a = "+a);
        return a + b;
    };


f1.applyAsInt(7,5); // calls the function of the lambda expression


Arrays.sort(employees, new Comparator() {
        public int compare(Employee e1, Employee e2) {
            //
        }
    });

Arrays.sort(employees, (Employee e1, Employee e2) -> // );

public static <T> void sort(T[] array, comp) {
    
    if (comp.compare(array[i-1], array[i]) > 0) {
        // swap
    }
}



public interface PenaltyFunction {
    public long compute(long minBal, long bal);
}

//    long penalty = 500;
    PenaltyFunction penalty = 

We have a standard set of functional interfaces, which are available
in java.util.function package, these cover the most commonly used
method signatures.
    Runnable                - public void run()
java.util.function has 43 functional interfaces.
return type void are consumers:
    Consumer<T>             - public void accept(T t)
    IntConsumer
    LongConsumer
    DoubleConsumer
    BiConsumer<T,U>
    ObjIntConsumer<T>       - public void accept(T t, int a)
    ObjLongConsumer<T>
    ObjDoubleConsumer<T>
all of them have the method called accept
no parameter called as suppliers
    Supplier<T>         - public T get()
    IntSupplier         - public int getAsInt()
    LongSupplier
    DoubleSupplier
    BooleanSupplier
method name starts with get and depends on return type

return type boolean predicates
    Predicate<T>        - public boolean test(T t)
    IntPredicate        - public boolean test(int a)
    LongPredicate
    DoublePredicate
    BiPredicate<T,U>    - public boolean test(T t, U u)
method name is test

methods having input parameters as well as a return type Function:
    Function<T,R>       - public R apply(T t)
    IntToLongFunction   - public long applyAsLong(int a)
    IntToDoubleFunction
    LongToIntFunction
    LongToDoubleFunction
    DoubleToIntFunction
    DoubleToLongFunction
    ToIntFunction<T>    - public int applyAsInt(T t)
    ToLongFunction<T>
    ToDoubleFunction<T>
    IntFunction<R>      - public R apply(int a)
    LongFunction<R>
    DoubleFunction<R>
    BiFunction<T,U,R>   - public R apply(T t, U u)
    ToIntBiFunction<T,U> - public int applyAsInt(T t, U u)
    ToLongBiFunction<T,U>
    ToDoubleBiFunction<T,U>
special case of Function types where input and output types match
    UnaryOperator<T>
    IntUnaryOperator
    LongUnaryOperator
    DoubleUnaryOperator
    BinaryOperator<T>
    IntBinaryOperator
    LongBinaryOperator
    DoubleBinaryOperator




------------------------------------------------------
Date: 10-May-2024

String conversion for reference types.
SomeRefType x = ...;
x+""

Misc. classes and Intro to Streams

Comparator<T>
static methods

public static <T,U extends Comparable<U>> Comparator<T> 
        comparing(Function<T,U> mapping)

Comparator<Account> byName = Comparator.comparing(Account::getName);
Arrays.sort(accounts, byName);

public static <T,U> Comparator<T> 
        comparing(Function<T,U> mapping, Comparator<U> comp)

Comparator<Account> byNameIgnoringCase = 
        Comparator.comparing(Account::getName,
             String.CASE_INSENSITIVE_ORDER);

Arrays.sort(accounts, byNameIgnoringCase);

public static <T> Comparator<T> comparingInt(ToIntFunction<T> mapper)
public static <T> Comparator<T> comparingLong(ToLongFunction<T> mapper)
public static <T> Comparator<T> comparingDouble(ToDoubleFunction<T> mapper)

Comparator<Account> byBalance = 
        Comparator.comparingLong(ac -> ac.getBalance());

Comparator<Account> byBalance = 
        Comparator.comparingLong(Account::getBalance);


public static <T extends Comparable<T>> Comparator<T> naturalOrder()
public static <T extends Comparable<T>> Comparator<T> reverseOrder()

String[] names = {"James", "Joshua", "John", "Tom", null, "Dick", null, "Harry"};

creating null-friendly Comparator
public static <T> Comparator<T> nullsFirst(Comparator<T> c)
public static <T> Comparator<T> nullsLast(Comparator<T> c)

default methods of Comparator<T>

public default Comparator<T> thenComparing(Comparator<T> comp)

Comparator<Account> byNameThenBalance = byName.thenComparing(byBalance)

public default <U extends Comparable<U>> Comparator<T> thenComparing(Function<T,U> mapper)

public default Comparator<T> reverse()

a useful method in Object class.
public final Class getClass()
The method returns the Class instance for given object.

we can have a Class object for any data-type by using a notation.

Class c1 = Account.class;
Class c2 = int.class;
Class c3 = int[].class;

Account ac = new ...;
ac.getClass() == CurrentAccount.class

----------------


public class Account {
    //
    private String jointHolderName;
/*
    public String getJointHolderName() {
        return this.jointHolderName;
    }
*/
    public Optional<String> getJointHolderName() {
        return 
    }
}


Account ac = ....

    
System.out.println("Joint Holder:"+
    ac.getJointHolderName().orElse("Not available").toLowerCase());

    ac.getJointHolderName().ifPresent(s -> System.out.println("Joint Holder:"+s));

    ac.getJointHolderName().ifPresentOrElse(s -> System.out.println("Joint Holder:"+s), () -> System.out.println("Not available"));



public static <T> Optional<T> max(T[] array, Comparator<? super T> comp) {
    if (array.length = 0) return Optional.empty();
    result = array[0];
    for (int i = 1; i < array.length; i++) {
        ...
    }
    return Optional.of(result);
}

Optional<T>
static methods
public static <T> Optional<T> empty()
public static <T> Optional<T> of(T t)   // will throw excepiton if null
public static <T> Optional<T> ofNullable(T t)


---------------------------
Stream, part of java.util.stream package.

BaseStream

Stream<T>
IntStream
LongStream
DoubleStream

static methods for creating a Stream<T>
public static <T> Stream<T> iterate(T seed, UnaryOperator<T> function)
    seed,   function.apply(seed), 
    function.apply(function.apply(seed))
    
    x,  f(x),   f(f(x)), ...


some methods of Collection<E> added in Java 8.

public default void forEach(Consumer<E> action)
public default Stream<E> stream()
public default Stream<E> parallelStream()
public default void removeIf(Predicate<E> test)

for (Account ac : accountMap.values()) {
    System.out.println(ac);

}

accountMap.values().forEach(System.out::println);
accountMap.values().forEach(System.out::println);



-------------------------------------------------------
Date: 11-May-2024

link for downloading java documentation
https://www.oracle.com/java/technologies/javase-jdk21-doc-downloads.html

Advanced Usage of Stream
reduce and collect methods of Stream<T>

public class SomeClass {
    //
    public void method() {
        
        IntBinaryOperator op1 = (a, b) -> {
            
        }
        
        
    }
}
final variable
effectively final variable  - Java 8

class A {
    int x;
    private Account account;
    class B {   // inner class
    }
    static class C {    // top-level nested class
    }
    public void method1(int x) {
        final int[] local = new int[1];
//        local += x;
        class D extends Account {   // local class
            @Override
            public void deposit(long amt) {
                // using local[0] = 
                
            }
        }
        this.account = new D();
    }
    public void method2() {
        this.account.deposit(1_000);
    }
    public static void main(String[] args) {
        A a = new A();
        a.method1(23);
        a.method2();
    }
}

----------------------------------------
Stream<T>
intermediate operations:
public Stream<T> filter(Predicate<T> test)
public Stream<T> limit(long count)
public Stream<T> takeWhile(Predicate<T> test)
public Stream<T> skip(long count)
public Stream<T> dropWhile(Predicate<T> test)
public Stream<T> distinct()

public <U> Stream<U> map(Function<T,U> mapper)
public IntStream mapToInt(ToIntFunction<T> mapper)
public LongStream mapToLong(ToLongFunction<T> mapper)
public DoubleStream mapToDouble(ToDoubleFunction<T> mapper)
public Stream<T> sequential()
public Stream<T> parallel()
public Stream<T> sorted()
public Stream<T> sorted(Comparator<T> comp)
public Stream<T> onClose(Runnable closeHandler)

public <U> Stream<U> flatMap(Function<T,Stream<U>> mapper)
flatMapToInt
flatMapToLong
flatMapToDouble
public <R> Stream<R> multiMap(BiConsumer<T, Consumer<R>> consumer)


Stream<Transaction> transStream = bank.getAccountStream()
    .flatMap(Account::getTransactionStream);

Stream<Transaction> transStream = bank.getAccountStream()
    .multiMap((ac, cons) -> {
            for (Transaction tr : ac.getPassbook()) {
                cons.accept(tr);
            }
        });


What is a Spliterator<E>?
Iterator<E> we know

Spliterator is an Iterator which can be split.

one of the methods of Spliterator<E> is

    public Spliterator<E> trySplit()

Spliterator<E> sp2 = sp1.trySplit();

streams can be sequential or parallel
in parallel streams it will attempt to use the Spliterator.


terminal operations:

allMatch
anyMatch
noneMatch

public long count()
public Optional<T> min(Comparator<T> comp)
public Optional<T> max(Comparator<T> comp)

public Optional<T> findFirst()
public Optional<T> findAny()

public void forEach(Consumer<T> action)
public void forEachOrdered(Consumer<T> action)

public Object[] toArray()
public <A> A[] toArray(IntFunction<A[]> creator)

public List<T> toList()

Account[] allAccounts = bank.getAccountStream()
//    .toArray(size -> new Account[size]);
    .toArray(Account[]::new);

we have reduce and collect methods:
public T reduce(T initial, BinaryOperator<T> accumulator)
public Optional<T> reduce(BinaryOperator<T> accumulator)
public <U> U reduce(U initial, BiFunction<U,T,U> accumulator,
                BinaryOperator<U> combiner)

public <R> R collect(Supplier<R> supplier, BiConsumer<R,T> accumulator,
                BiConsumer<R,R> combiner)
public <A,R> R collect(Collector<T,A,R> collector)

public int sum(int[] array) {
    int result = 0;
    for (int value : array) {
        result = result + value;
    }
    return result;
}

public int product(int[] array) {
    int result = 1;
    for (int value : array) {
        result = result * value;
    }
    return result;
}

public int arrayProcess(int initial, IntBinaryOperator accumulator, int[] array) {
    int result = initial;
    for (int value : array) {
        result = accumulator.applyAsInt(result, value);
    }
    return result;
}
public int sum(int[] array) {
    return arrayProcess(0, (a,b) -> a + b);
}
public int product(int[] array) {
    return arrayProcess(1, (a,b) -> a * b);
}


public T reduce(T initial, BinaryOperator<T> accumulator)

public Optional<T> reduce(BinaryOperator<T> accumulator)

public <U> U reduce(U initial, BiFunction<U,T,U> accumulator,
                BinaryOperator<U> combiner)



public <R> R collect(Supplier<R> supplier, BiConsumer<R,T> accumulator,
                BiConsumer<R,R> combiner)


LongSummaryStatistics balanceStats = bank.getAccountStream()
        mapToLong(Account::getBalance)
        .collect(LongSummaryStatistics::new, 
                 LongSummaryStatistics::accept,
                 LongSummaryStatistics::combine);


public <A,R> R collect(Collector<T,A,R> collector)
Collector
    Supplier<A> supplier
    BiConsumer<A,T> accumulator
    BinaryOperator<A> combiner
    Function<A,R> finisher

Collector<T,A,R>
public static <T,A,R> Collector<T,A,R> of(
                        Supplier<A> supplier,
                        BiConsumer<A,T> accumulator,
                        BinaryOperator<A> combiner,
                        Function<A,R> finisher,
                        Collector.Characteristics... characteristics)

to process each part
1)  accumulate each part
    A a = supplier.get();
    for (T t : ...) {
        accumulator.accept(a, t);
    }
    // a
2)  combine the accumulated parts
    A... accValues
    A result = accValues[0];
    for (int i = 1; i < accValues.length; i++) {
        result = combiner.apply(result, accValues[i]);
    }
    result
3)  use finisher for returning
    return finisher.apply(result);

for standard collectors we have static methods in the Collectors class.
Using methods of Collectors class to create Collector


Account ac = ....

Double transAverage = ac.getTransactionStream()
        .collect(Collectors.summingLong(Transaction::getNetAmount));


Some methods of Collectors class
public <T,K> Collector<T,?,Map<K,List<T>>> groupingBy(Function<T,K> classifier)

Map<String,List<Account>> byName = bank.getAccountStream().collect(
    Collectors.groupingBy(Account::getName));

Map<String,Long> balanceSumByName = bank.getAccountStream().collect(
    Collectors.groupingBy(Account::getName,
         Collectors.summingLong(Account::getBalance)));


Map<String,Long> balanceSumByName = bank.getAccountStream().collect(
    Collectors.groupingBy(Account::getName,
        TreeMap::new,
        Collectors.summingLong(Account::getBalance)));

Map<String,Long> balanceSumByName = bank.getAccountStream().collect(
    Collectors.toMap(Account::getName, Account::getBalance,
        (currBal, bal) -> currBal + bal)
    );


email: pravin@zensoftech.co.in
YouTube channel: youtube.com/@classofjava
PlayList: JavaGU2024
          Java2204

















